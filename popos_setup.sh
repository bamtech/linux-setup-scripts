#!/bin/bash

# Remove some unwanted ish
apt remove -y libreoffice*

# Iinstall system tools
apt install -y zsh neofetch bpytop flatpak

# Install dev tools
apt install -y openjdk-19-jdk-headless maven

# Install container tools
apt install -y podman podman-toolbox

# Install flatpak apps
sh ./flatpak_apps.sh
