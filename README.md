# Linux OS Setup

Just an easy way to install some software because I tinker too much and reinstall distributions of everything. 

- [Linux OS Setup](#linux-os-setup)
  - [To use:](#to-use)
    - [Fedora](#fedora)
    - [Pop!\_OS](#pop_os)

**What the scripts do in common:**
1. Removes some unwanted packages
2. Installs systems tools
3. Installs dev tools
4. Adds flathub repo
5. Installs flatpak apps by category
## To use:

**1. Clone this repo:**

```bash 
git clone https://gitlab.com/bamtech/linux-setup-scripts.git
```

**2. Make any desired changes to the apps to be installed with your favorite text editor**

**3. Use the script for the OS you're on:**

### Fedora

```bash
cd linux-setup-scripts/ && sudo sh fedora_setup.sh
```

**The fedora script adds some extras since they are not in the base operating system:** 
1. Installs rpm fusion repositories (free & non-free)
2. Installs nvidia drivers
3. Installs multimedia/streaming codecs

### Pop!_OS

```bash
cd linux-setup-scripts/ && sudo sh popos_setup.sh
```