#!/bin/bash

# Add Flatpak Repo - flathub
flatpak remote-add --user --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo

# Install browsers
flatpak install -y flathub com.brave.Browser \
    io.gitlab.librewolf-community \
    com.github.micahflee.torbrowser-launcher
    

# Install Communications
flatpak install -y flathub org.signal.Signal \
    com.slack.Slack

# Install Office and Productivity
flatpak install -y flathub com.bitwarden.desktop \
    org.onlyoffice.desktopeditors \
    md.obsidian.Obsidian

# Install Entertainment and other ish
flatpak install -y flathub com.spotify.Client \
    org.qbittorrent.qBittorrent

# Multimedia
flatpak install -y flathub io.github.celluloid_player.Celluloid \
    net.filebot.FileBot \
    fr.handbrake.ghb \
    com.github.unrud.VideoDownloader

# Gaming
flatpak install -y flathub com.valvesoftware.Steam \
    com.valvesoftware.SteamLink

# GNOME specific
flatpak install -y flathub com.mattjakeman.ExtensionManager
