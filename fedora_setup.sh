#!/bin/bash

# Remove unwanted stuff
dnf remove -y gnome-maps gnome-tour gnome-connections rhythmbox cheese libreoffice*

# Add RPM Fusion Repos (free & non-free)
dnf install -y https://mirrors.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm https://mirrors.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm
dnf update -y

# Nvidia Drivers
dnf install -y akmod-nvidia xorg-x11-drv-nvidia-cuda

# Add multimedia groups
dnf groupupdate -y core
dnf groupupdate -y multimedia --setop="install_weak_deps=False" --exclude=PackageKit-gstreamer-plugin
dnf groupupdate -y sound-and-video

# System Tools
dnf install -y fish neofetch bpytop

# Install Java and Maven
dnf install -y java-latest-openjdk-headless maven

# Install flatpak apps
sh ./flatpak_apps.sh
